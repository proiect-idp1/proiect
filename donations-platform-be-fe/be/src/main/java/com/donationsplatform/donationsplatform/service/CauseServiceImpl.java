package com.donationsplatform.donationsplatform.service;

import com.donationsplatform.donationsplatform.dto.Cause;
import com.donationsplatform.donationsplatform.repository.CauseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CauseServiceImpl implements CauseService {

    @Autowired
    private CauseRepository causeRepository;

    @Override
    public Cause saveCause(Cause cause) {
        cause.setAmountRaised((double) 0);
        return causeRepository.save(cause);
    }

    @Override
    public List<Cause> getAllCauses() {
        return causeRepository.findAll();
    }

    @Override
    public Optional<Cause> getCauseById(int id) {
        return causeRepository.findById(id);
    }

    @Override
    public void calculateAmountRaised(int id, Double amount) {
        Cause cause = causeRepository.findById(id).orElseThrow();
        cause.setAmountRaised(cause.getAmountRaised() + amount);
        causeRepository.save(cause);
    }

    @Override
    public void updateCause(Cause cause) {
        Cause cause1 = causeRepository.findById(cause.getId()).orElseThrow();
        cause1.setZone(cause.getZone());
        cause1.setTitle(cause.getTitle());
        cause1.setTarget(cause.getTarget());
        cause1.setSummary(cause.getSummary());
        cause1.setImage(cause.getImage());
        causeRepository.save(cause1);
    }
}
