package com.donationsplatform.donationsplatform.service;

import com.donationsplatform.donationsplatform.dto.User;

import java.util.List;
import java.util.Optional;


public interface UserService {
    public User addUser(User user);
    public List<User> getAllUsers();
    public Optional<User> getUserById(int userId);
    public Optional<User> findByEmail(String email);
    public void updateUser(User user);
}
