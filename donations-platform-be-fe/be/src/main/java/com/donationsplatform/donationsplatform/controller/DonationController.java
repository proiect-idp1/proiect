package com.donationsplatform.donationsplatform.controller;


import com.donationsplatform.donationsplatform.dto.Donation;
import com.donationsplatform.donationsplatform.dto.User;
import com.donationsplatform.donationsplatform.service.DonationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/donation")
public class DonationController {

    @Autowired
    private DonationService donationService;

    @PostMapping("/addDonation")
    @ResponseBody
    public ResponseEntity addDonation(@RequestBody Donation donation) {
        donationService.saveDonation(donation);
        return new ResponseEntity("Donation added succesfully!", HttpStatus.OK);
    }

    @GetMapping("/getAllDonations")
    @ResponseBody
    public ResponseEntity<List<Donation>> getAllCauses() {
        return new ResponseEntity<>(donationService.getAllDonations(), HttpStatus.OK);
    }

    @GetMapping("/getDonation/{userId}")
    @ResponseBody
    public List<Donation> getAllDonationsForUser(@PathVariable int userId){
        return donationService.getAllDonationsForUser(userId);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/getDonation/")
    public List<Donation> getUserByEmail(@RequestParam(value="email") String email) {
        return  donationService.getAllDonationsForUserByEmail(email);
    }
}
