package com.donationsplatform.donationsplatform.repository;

import com.donationsplatform.donationsplatform.dto.Donation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DonationRepository extends JpaRepository<Donation, Integer> {

    List<Donation> findAllByUserId(int userId);

    @Query(value = "SELECT d FROM Donation d JOIN User u on d.userId = u.id and u.email =:email")
    public List<Donation> findAllDonationsForUser(@Param("email") String email);
}
