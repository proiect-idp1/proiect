package com.donationsplatform.donationsplatform.service;

import com.donationsplatform.donationsplatform.dto.Cause;

import java.util.List;
import java.util.Optional;

public interface CauseService {

        public Cause saveCause(Cause cause);
        public List<Cause> getAllCauses();
        public Optional<Cause> getCauseById(int id);
        public void calculateAmountRaised(int id, Double amount);
        public void updateCause(Cause cause);
}
