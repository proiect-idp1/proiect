package com.donationsplatform.donationsplatform.service;

import com.donationsplatform.donationsplatform.dto.Donation;

import java.util.List;

public interface DonationService {

    public Donation saveDonation(Donation donation);

    public List<Donation> getAllDonations();

    List<Donation> getAllDonationsForUser(int userId);

    List<Donation> getAllDonationsForUserByEmail(String email);
}
