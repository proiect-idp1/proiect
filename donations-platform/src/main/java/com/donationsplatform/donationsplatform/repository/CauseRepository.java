package com.donationsplatform.donationsplatform.repository;

import com.donationsplatform.donationsplatform.dto.Cause;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CauseRepository extends JpaRepository<Cause, Integer> {
}
