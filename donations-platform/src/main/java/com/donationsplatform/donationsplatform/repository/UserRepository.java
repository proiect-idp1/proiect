package com.donationsplatform.donationsplatform.repository;

import com.donationsplatform.donationsplatform.dto.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
}
