package com.donationsplatform.donationsplatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DonationsPlatformApplication {

	public static void main(String[] args) {
		SpringApplication.run(DonationsPlatformApplication.class, args);
	}

}
