package com.donationsplatform.donationsplatform.service;

import com.donationsplatform.donationsplatform.dto.Donation;
import com.donationsplatform.donationsplatform.repository.DonationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DonationServiceImpl implements DonationService {

    @Autowired
    private DonationRepository donationRepository;

    @Override
    public Donation saveDonation(Donation donation) {
        return donationRepository.save(donation);
    }

    @Override
    public List<Donation> getAllDonations() {
        return donationRepository.findAll();
    }

    public List<Donation> getAllDonationsForUser(int userId) {
        return donationRepository.findAllByUserId(userId);
    }

}
