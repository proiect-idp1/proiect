package com.donationsplatform.donationsplatform.service;

import com.donationsplatform.donationsplatform.dto.Cause;
import com.donationsplatform.donationsplatform.repository.CauseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CauseServiceImpl implements CauseService {

    @Autowired
    private CauseRepository causeRepository;

    @Override
    public Cause saveCause(Cause cause) {
        return causeRepository.save(cause);
    }

    @Override
    public List<Cause> getAllCauses() {
        return causeRepository.findAll();
    }

    @Override
    public Optional<Cause> getCauseById(int id) {
        return causeRepository.findById(id);
    }
}
