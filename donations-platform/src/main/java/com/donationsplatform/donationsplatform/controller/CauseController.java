package com.donationsplatform.donationsplatform.controller;

import com.donationsplatform.donationsplatform.dto.Cause;
import com.donationsplatform.donationsplatform.service.CauseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/v1/cause")
public class CauseController {

    @Autowired
    private CauseService causeService;

    @PostMapping("/addCause")
    @ResponseBody
    public ResponseEntity addCause(@RequestBody Cause cause) {
        causeService.saveCause(cause);
        return new ResponseEntity("Cause added succesfully!", HttpStatus.OK);
    }

    @GetMapping("/getAllCauses")
    @ResponseBody
    public ResponseEntity<List<Cause>> getAllCauses() {
        return new ResponseEntity<>(causeService.getAllCauses(), HttpStatus.OK);
    }

    @GetMapping("/getCause/{id}")
    @ResponseBody
    public ResponseEntity<Optional<Cause>> getCauseById(@PathVariable int id) {
        return new ResponseEntity<>(causeService.getCauseById(id), HttpStatus.OK);
    }

}
