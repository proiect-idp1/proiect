package com.donationsplatform.donationsplatform.controller;

import com.donationsplatform.donationsplatform.dto.Donation;
import com.donationsplatform.donationsplatform.dto.User;
import com.donationsplatform.donationsplatform.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/registerUser")
    @ResponseBody
    public ResponseEntity addUser(@RequestBody User user) {
        userService.addUser(user);
        return ResponseEntity.ok().body("User added succesfully!");
    }

    @GetMapping("/viewUserData")
    @ResponseBody
    public ResponseEntity<List<User>> getAllUsers() {
        return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
    }

    @GetMapping("/viewUserData/{userId}")
    @ResponseBody
    public ResponseEntity<Optional<User>> getUserById(@PathVariable int userId) {
        return new ResponseEntity<>(userService.getUserById(userId), HttpStatus.OK);
    }
}
